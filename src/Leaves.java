import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Leaves {

	public static void main(String[] args) throws FileNotFoundException {
		
		Scanner fileInput = new Scanner(new File("LeavesIN.txt"));
		 
		while(fileInput.hasNext()) {

			String[] nextLine = fileInput.nextLine().split(",");
			int[] diceRolls = new int[nextLine.length];
			for(int i = 0; i < nextLine.length; i++) {
				diceRolls[i] = Integer.parseInt(nextLine[i]);
			}
			
			System.out.println(calculateLeaves(diceRolls));
		}
		
	}
	
	public static int calculateLeaves(int[] arr) {
		int v1=0;
		int v2=0;
		int v3=2;
		int v4=0;
		int v5=4;
		int v6=0;
		int sum = 0;
		for(int n : arr)
		{
			if(n == 1)
			{
				sum += v1;
			}
			if(n == 2)
			{
				sum += v2;
			}
			if(n == 3)
			{
				sum += v3;
			}
			if(n == 4)
			{
				sum += v4;
			}
			if(n == 5)
			{
				sum += v5;
			}
			if(n == 6)
			{
				sum += v6;
			}
		}
		return sum;
	}
}
